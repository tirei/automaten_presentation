#+TITLE: Automaten
#+AUTHOR: Emir Mirvic und Timon Reinold
#+LANGUAGE: de
#+DATE: 2019-08-19

#+OPTIONS: toc:nil num:nil
#+REVEAL_INIT_OPTIONS: transition:'none', slideNumber:'c/t', progress:false
#+REVEAL_THEME: white
#+REVEAL_HLEVEL: 2
#+REVEAL_EXTRA_CSS: ./local.css
#+REVEAL_TITLE_SLIDE: <h3> %t </h3> <small> %a </small>

* Endlicher Automat
[[./img/finite.png]]

#+BEGIN_NOTES
- Akzeptor
- Sprachenerkennen <-> Berechenbarkeit
- Beispiel/Funktionsweise erklären
#+END_NOTES

** Definition
Endlicher Automat $A$:
- endliche, nicht leere Menge der Zustände $Z$
- Eingabealphabet $E$
- Übergangsfunktion (auch Überführungsfunktion) $f: Z × E → Z$
- Startzustand $z_0 ∈ Z$
- Menge an Endzuständen $Z_E ⊆ Z$

#+BEGIN_NOTES
- formale Definition
#+END_NOTES

* Kellerautomaten
[[./img/pda.png]]


#+BEGIN_NOTES
- Keller
- Äquivalenz kontextfreie Grammatiken
- Beispiel/Funktionsweise erklären
#+END_NOTES

** Definition
Kellerautomat $K$:
- endliche, nicht leere Menge an Zuständen $Q$
- Eingabealphabet $Σ$
- Alphabet der Kellersymbole $Γ$
- Übergangsfunktion $f: Q × (Σ ∪ \{λ\}) × Γ → Q × Γ^*$
- Startzustand $q_0 ∈ Q$
- Startkellersymbol $Z ∈ Γ$
- Menge an Endzuständen $F ⊆ Q$

#+BEGIN_NOTES
- formale Definition
#+END_NOTES

* Turingmaschine
[[./img/turing.png]]

#+BEGIN_NOTES
- Äquivalenz (Lambda-Kalkül, Registermaschine, While-Berechenbarkeit, Grammatiken)
- Band mit Lese-/Schreibkopf
- Beispiel/Funktionsweise erklären
#+END_NOTES

** Definition
Turingmaschine $T$:
- endliche, nicht leere Menge an Zuständen $Q$
- Bandalphabet $Γ$
- Symbol der leeren Zelle $∅ ∈ Γ$
- Eingabealphabet $Σ ⊂ Γ$ mit $∅ ∉ Σ$
- Übergangsfunktion $f: Q × Γ → Q × Γ × \{R, L\}$
- Anfangszustand $q_0 ∈ Q$
- Menge an Endzuständen $F ⊆ Q$

#+BEGIN_NOTES
- formale Definition
#+END_NOTES

** Universelle Turingmaschine

#+BEGIN_NOTES
- simuliert encodierte TM
#+END_NOTES

** Abzählbarkeit

#+BEGIN_NOTES
- TM abzählbar
- $f: ℕ → ℕ$ überabzählbar
- => Unberechenbarkeit
  - Halteproblem
  - Fleißige Biber
#+END_NOTES

** Varianten

#+BEGIN_NOTES
- 2D statt 1D
- Band mit einem Ende
- Stehenbleiben
- mehrere seq. Aktionen in einem Schritt
#+END_NOTES

* Determinismus

#+BEGIN_NOTES
- genau 1 Aktion pro Schritt
- bei endlichen Automaten gleichmächtig
  - nicht bei Kellerautomat
#+END_NOTES

* Quellen
  :PROPERTIES:
  :reveal_background: #000000
  :END:
- Aufschriebe des Informatikunterrichtes
- INF-SCHULE
  - Sprachen und Automaten
  - Berechenbarkeit
- Wikipedia DE
  - Turingmaschine (Benennungen in den Definitionen)
