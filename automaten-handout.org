#+TITLE: Automaten
#+AUTHOR: Emir Mirvic und Timon Reinold
#+LANGUAGE: de
#+DATE: 2019-08-19
#+OPTIONS: num:nil toc:nil
#+LATEX_HEADER: \usepackage{newunicodechar}
#+LATEX_HEADER: \newunicodechar{Γ}{\Gamma}
#+LATEX_HEADER: \newunicodechar{Σ}{\Sigma}
#+LATEX_HEADER: \newunicodechar{λ}{\lambda}
#+LATEX_HEADER: \newunicodechar{×}{\times}
#+LATEX_HEADER: \newunicodechar{∈}{\in}
#+LATEX_HEADER: \newunicodechar{∉}{\notin}
#+LATEX_HEADER: \newunicodechar{→}{\to}
#+LATEX_HEADER: \newunicodechar{⊂}{\subset}
#+LATEX_HEADER: \newunicodechar{⊆}{\subseteq}
#+LATEX_HEADER: \newunicodechar{∅}{\emptyset}
#+LATEX_HEADER: \newunicodechar{ℕ}{\mathbb{N}}
#+LATEX_HEADER: \newunicodechar{∪}{\cup}
 

* Automaten
Ein Automat (auch Akzeptor) zeigt an, ob ein Wort zu seiner Sprache gehört. So
sind Automaten eine Möglichkeit potenziell unendliche Sprachen durch endliche
Konstrukte zu beschreiben.

Da sich Probleme der Berechenbarkeit durch dieses Akzeptieren von Wörtern
ausdrücken lassen, sind sie wichtige Modelle der theoretischen Informatik.

* Endlicher Automat
Der endliche Automat ist eine Grundlage der behandelten Automaten und hat von
ihnen die geringste Mächtigkeit. Die Menge der durch endliche Automaten
beschreiben Sprachen ist die Menge der regulären Sprachen, also der Sprachen,
für die eine Grammatik existiert, deren Produktionen alle links ein einziges
Nichtterminalsymbol und rechts entweder $λ$ oder ein Terminalsymbol, gefolgt von
einem Nichtterminalsymbol haben.

** Definition
Ein endlicher Automat $A$ ist ein Quintupel aus 
- der endlichen, nicht leeren Menge der Zustände $Z$,
- dem Eingabealphabet $E$,
- der Übergangsfunktion (auch Überführungsfunktion) $f: Z × E → Z$,
- dem Startzustand $z_0 ∈ Z$ und
- der Menge an Endzuständen $Z_E ⊆ Z$.

** Funktionsweise
Für jedes Symbol des Eingabewortes (in Reihenfolge) wird durch $f$
- dem jetzigen Zustand (beginnend mit $z_0$) und
- dem jeweiligen Eingabesymbol
ein neuer Zustand zugeordnet, der für das nächste Symbol verwendet werden kann.

Das Eingabewort ist genau dann $∈ L(A)$, wenn der letzte so erhaltene Zustand $∈
Z_E$ ist.

** Beispiel
[[./img/finite.png]]
Dieser Automat erkennt diese Sprache (i.e. beliebig viele "=a="s, gefolgt von
mindestens einem "=b="):
#+begin_src
wort = { "a" }, "b", { "b" };
#+end_src

* Kellerautomaten
Ein Kellerautomat ist eine Erweiterung des endlichen Automats, der zusätzlich
einen Keller (einen Stack) zur Verfügung hat und somit manche Informationen
zwischenspeichern kann. Die durch alle Kellerautomaten beschriebenen Sprachen
sind die kontextfreien Sprachen, also die Sprachen, für die eine Grammatik
existiert, deren Produktionen alle links nur ein Nichtterminalsymbol haben.

** Definition
Ein Kellerautomat $K$ ist ein Septupel aus
- einer endlichen, nicht leeren Menge an Zuständen $Q$,
- dem Eingabealphabet $Σ$,
- dem Alphabet der Kellersymbole $Γ$,
- der Übergangsfunktion $f: Q × (Σ ∪ \{λ\}) × Γ → Q × Γ^*$,
- dem Startzustand $q_0 ∈ Q$,
- dem Startkellersymbol $Z ∈ Γ$ und
- der Menge an Endzuständen $F ⊆ Q$.

** Funktionsweise
Solange das Eingabewort noch nicht vollständig abgearbeitet ist, wird von $f$
- dem jetzigen Zustand (beginnend mit $q_0$),
- dem obersten Symbol im Keller (beginnend mit $Z$), welches dabei konsumiert
  wird und
- dem nächsten Eingabesymbol $∈ Σ$ (welches dabei konsumiert wird) oder $λ$
zugeordnet
- der nächste Zustand $∈ Q$ und
- ein Wort $∈ Γ^*$, welches an den Keller angefügt wird.

Das Eingabewort ist genau dann $∈ L(K)$, wenn der letzte so erhaltene Zustand $∈
F$ ist.

Alternativ ist es genau dann $∈ L(K)$, wenn nach der Verarbeitung des kompletten
Eingabewortes der Keller leer ist. Diese beiden Definitionen sind äquivalent.

** Beispiel
[[./img/pda.png]]
Dieser Kellerautomat erkennt diese Sprache (i.e. genestete Klammern):
#+begin_src
wort = ["(", wort, ")"];
#+end_src

* Turingmaschine
Die Turingmaschine ist mächtiger als beide bereits vorgestellten Automaten. Sie
ist aber genauso mächtig wie einige andere Modelle wie Grammatiken, dem
Lambda-Kalkül, der Registermaschine und der While-Berechenbarkeit.

Statt einem Keller besitzt sie ein in beide Richtungen unendliches Band, welches
an einer Stelle ausgelesen und beschrieben werden kann, die entlang dieses
Bandes verschoben werden kann.

** Definition
Eine Turingmaschine $T$ ist ein Septupel aus
- der endlichen, nicht leeren Menge an Zuständen $Q$,
- dem Bandalphabet $Γ$,
- dem Symbol der leeren Zelle $∅ ∈ Γ$,
- dem Eingabealphabet $Σ ⊂ Γ$ mit $∅ ∉ Σ$,
- der Übergangsfunktion $f: Q × Γ → Q × Γ × \{R, L\}$,
- dem Anfangszustand $q_0 ∈ Q$ und
- der Menge an Endzuständen $F ⊆ Q$.

** Funktionsweise
Das Band wird mit dem Eingabewort initialisiert, die restlichen Zellen werden
mit $∅$ beschrieben. Solange $T$ nicht in einem Zustand $∈ F$ ist, wird durch $f$
- dem jetzigen Zustand (beginnend mit $q_0$) und
- dem momentan zu lesendem Bandsymbol
zugeordnet
- der nächste Zustand $∈ Q$,
- das Bandsymbol $∈ Γ$, welches auf das Band geschrieben wird und
- $R$ oder $L$, wonach die Lese-/Schreibstelle verschoben wird.

Das Eingabewort ist genau dann $∈ L(T)$, wenn $T$ einen Zustand $∈ F$ erreicht.

** Beispiel
[[./img/turing.png]]
Diese Turingmaschine erkennt die Sprache $L = \{a^n b^n c^n | n ∈ ℕ\}$.

** Universelle Turingmaschine
Es existiert eine Turingmaschine, die jede andere Turingmaschine ausführen kann.
Hierfür wird die encodierte Turingmaschine mitsamt ihres Eingabewortes auf das
Band der universellen Turingmaschine geschrieben, welche dies genau dann
akzeptiert, wenn die simulierte Turingmaschine das Eingabewort akzeptieren
würde.

** Abzählbarkeit
Da es möglich ist Turingmaschinen den natürlichen Zahlen die Menge aller
Turingmaschinen zuzuordnen (vgl. eine Encodierung der Turingmaschine für die
Universelle Turingmaschine), ist ihre Menge abzählbar. Die Menge der Funktionen
$f: ℕ → ℕ$ ist überabzählbar. Folglich existieren Funktionen, die nicht von
einer Turingmaschine berechnet werden können.

Beispiele hierfür sind das Halteproblem und die Zuordnung der Anzahl der
Zustände eines Fleißigem Bibers zu der Anzahl der geschrieben Bandsymbole.

** Varianten
Es existieren mehrere Varianten der Turingmaschine, die jedoch alle die gleiche
Mächtigkeit besitzen, e.g.:
- ein zweidimensionales statt einem eindimensionalem Band
- ein Band, welches in nur eine Richtung endlos ist
- den Bewegungsoperatoren $\{R, N, L\}$ statt $\{R, L\}$, also zuzüglich
  /Stehenbleiben/
- mit mehreren sequentiell ausgeführten Aktionen pro Schritt

* Determinismus
Ein Automat ist genau dann deterministisch, wenn zu jedem Schritt genau eine
Aktion möglich ist. Bei einem nicht-deterministischen Automaten kann die
Übergangsfunktion stattdessen zu einer endlichen Menge an /Aktionen/ zuordnen.

Deterministische und nicht-deterministische endliche Automaten sind gleich
mächtig. Für Kellerautomaten gilt dies nicht.

* Quellen
- Aufschriebe des Informatikunterrichtes
- INF-SCHULE (https://inf-schule.de, 2019-08-26)
  - Sprachen und Automaten
  - Berechenbarkeit
- Wikipedia DE (https://de.wikipedia.org, 2019-08-26)
  - Turingmaschine (Benennungen in den Definitionen)
