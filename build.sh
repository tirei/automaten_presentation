#!/bin/sh

emacs --batch \
      --eval "(progn
  (require 'org)
  (require '$1)
  (find-file \"automaten-handout.org\")
  ($2))"
